import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit, OnChanges {
  @Input() totalRecords;  
  @Input() recordsPerPage;  
  
  @Output() onPageChange: EventEmitter<any> = new EventEmitter();  
  activePage:number;  
  constructor() { }

  ngOnInit(): void {
    this.activePage = 0;
  }

  pageSizeChanged(pageSize){
    this.recordsPerPage = parseInt(pageSize);
    this.pageChange();
  }

  pageChange(){
    this.activePage = 0;  
    this.onPageChange.emit({page:this.activePage,recordsPerPage:this.recordsPerPage});  
  }
  isLastPage(){
    return (this.activePage+1) * this.recordsPerPage >= this.totalRecords;
  }
  ngOnChanges(){  
  }  

  onClickPage(pageNumber:number){
      this.activePage = pageNumber;  
      this.onPageChange.emit({page:this.activePage,recordsPerPage:this.recordsPerPage});  
  }  
}
