import { Component, OnInit } from '@angular/core';
import {CustomerService } from './customer.service';
@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit {
  customers;
  pageNumber = 0;
  recordsPerPage = 10;
  totalRecords;
  constructor(private customerService:CustomerService) { }

  ngOnInit(): void {
    const results = this.customerService.getData(this.recordsPerPage,this.pageNumber);
    this.totalRecords = results.total;
    this.customers = results.data;
  }
  pageChange(event){
    this.recordsPerPage = event.recordsPerPage;
    this.pageNumber = event.page;
    const results = this.customerService.getData(this.recordsPerPage,this.pageNumber);
    this.totalRecords = results.total;
    this.customers = results.data;
  }
  submit(cust){
    this.customerService.saveStatus(cust.id,cust.status)
    .subscribe(res=>console.log(res));
  }
}
